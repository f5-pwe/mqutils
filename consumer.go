package mqutils

import (
	"context"
)

const (
	DefaultRetryQueueTTL = 1000
)

type Consumer interface {
	Run(ctx context.Context) error
}
