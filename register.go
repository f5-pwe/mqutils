package mqutils

import (
	"sync"
)

var once = &sync.Once{}

func InitRegisters() {
	once.Do(func() {
		handlers = &sync.Map{}
	})
}
