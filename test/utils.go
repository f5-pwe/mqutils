package test

import (
	"testing"

	"github.com/go-kit/kit/log"
)

type testLogger struct {
	t *testing.T
}

func NewTestLogger(t *testing.T) log.Logger {
	t.Helper()
	return &testLogger{t}
}

func (tl *testLogger) Log(keyvals ...interface{}) error {
	tl.t.Helper()
	tl.t.Log(keyvals...)
	return nil
}
