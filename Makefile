.PHONY: all format test deps

format:
	go fmt ./...

test:
	go vet ./...
	go test -v -race ./...

deps:
	go mod tidy
	go mod verify