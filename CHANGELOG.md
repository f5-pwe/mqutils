

<!--- next entry here -->

## 2.0.1
2024-07-01

### Fixes

- address data race issues (207720ebfe02475d10dbb351bcb2dec29baab811)

## 2.0.0
2024-07-01

### Breaking changes

#### BREAKING CHANGE: refactor to have the mq specific connections as subpackages (65f801b1423335ff9c32b0af3b42393695f11fba)

refactor to have the mq specific connections as subpackages

## 1.0.0
2019-05-10

### Features

- Initial release of the mqutils package (fbf8535a00f4bc04430fc89f5ffa1f1896069bb0)

