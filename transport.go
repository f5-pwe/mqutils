package mqutils

import (
	"context"

	"github.com/go-kit/kit/log"
)

type Transport interface {
	Dial(url string, logger log.Logger) error
	Close() error
	Ack(Message) error
	Nack(Message) error
	DeclareExchange(string, string) error
	BindQueue(string, string, []string, map[string]interface{}) error
	Messages(context.Context, string) (<-chan Message, error)
}
