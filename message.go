package mqutils

import "github.com/go-kit/kit/log"

type Message interface {
	Ack() error
	Nack() error
	Logger(log.Logger) log.Logger
	RetryCount() int
}
