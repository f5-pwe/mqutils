package mqutils

import (
	"sync"
)

type Handler interface {
	Process(Message) error
	PreRun() error
	PostRun() error
	PreDelete(Message) error
}

type HandlerFunc func() Handler

var handlers *sync.Map

func RegisterHandler(name string, factory HandlerFunc) {
	InitRegisters()
	handlers.Store(name, factory)
}

func GetHandlerFunc(name string) (f HandlerFunc, ok bool) {
	ft, ok := handlers.Load(name)
	f = ft.(HandlerFunc)
	return
}

type DefaultHandler struct{}

func (h *DefaultHandler) Process(m Message) error {
	return nil
}

func (h *DefaultHandler) PreRun() error {
	return nil
}

func (h *DefaultHandler) PostRun() error {
	return nil
}

func (h *DefaultHandler) PreDelete(message Message) error {
	return nil
}
