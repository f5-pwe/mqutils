package amqp_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/stretchr/testify/require"
	"gitlab.com/f5-pwe/mqutils"
	"gitlab.com/f5-pwe/mqutils/amqp"
	"gitlab.com/f5-pwe/mqutils/test"
)

type cfgObj map[string]interface{}

func testLogger(t *testing.T) log.Logger {
	logger := test.NewTestLogger(t)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	return logger
}

// func TestNewAMQPConsumer(t *testing.T) {
// 	t.Parallel()
//
// 	mqutils.RegisterHandler(t.Name(), newHandler)
// 	consumer, err := amqp.NewAMQPConsumer(newCfg(t), testLogger(t))
// 	require.NoError(t, err)
// 	consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).waitFor(1)
//
// 	ctx, cancel := context.WithTimeout(context.Background(), 55*time.Second)
// 	t.Cleanup(cancel)
//
// 	wg := &sync.WaitGroup{}
// 	wg.Add(1)
// 	go func() {
// 		defer wg.Done()
// 		require.NoError(t, consumer.Run(ctx))
// 	}()
// 	send(t, rmq_host, t.Name())
// 	err = consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).wait()
// 	assert.NoError(t, err)
// 	cancel()
// 	wg.Wait()
// }

// func TestNewAMQPConsumerReconnect(t *testing.T) {
// 	t.Parallel()
//
// 	mqutils.RegisterHandler(t.Name(), newHandler)
// 	consumer, err := amqp.NewAMQPConsumer(newCfg(t), testLogger(t))
// 	require.NoError(t, err)
// 	consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).waitFor(2)
//
// 	ctx, cancel := context.WithTimeout(context.Background(), 55*time.Second)
// 	t.Cleanup(cancel)
//
// 	wg := &sync.WaitGroup{}
// 	wg.Add(1)
// 	go func() {
// 		defer wg.Done()
// 		require.NoError(t, consumer.Run(ctx))
// 	}()
//
// 	send(t, rmq_host, t.Name())
// 	time.Sleep(1 * time.Second)
// 	require.NoError(t, consumer.(*amqp.AMQPConsumer).Transport.(*amqp.AMQPTransport).Close())
// 	time.Sleep(1 * time.Second)
// 	send(t, rmq_host, t.Name())
// 	err = consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).wait()
// 	assert.NoError(t, err)
// 	cancel()
// 	wg.Wait()
// }

func TestNewAMQPConsumerExitClean(t *testing.T) {
	t.Parallel()

	mqutils.RegisterHandler(t.Name(), newHandler)
	consumer, err := amqp.NewAMQPConsumer(newCfg(t), testLogger(t))
	require.NoError(t, err)
	consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).waitFor(2)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		require.NoError(t, consumer.Run(ctx))
	}()

	cancel()
	wg.Wait()
}

func TestNewAMQPConsumerExitTimeout(t *testing.T) {
	t.Parallel()

	mqutils.RegisterHandler(t.Name(), newHandler)
	consumer, err := amqp.NewAMQPConsumer(newCfg(t), testLogger(t))
	require.NoError(t, err)
	consumer.(*amqp.AMQPConsumer).Handler.(*amqpHandler).waitFor(2)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	t.Cleanup(cancel)
	err = consumer.Run(ctx)
	require.Error(t, err)
}

func newCfg(t *testing.T) cfgObj {
	t.Helper()

	return cfgObj{
		"url":            rmq_host,
		"exchange":       t.Name(),
		"exchange_type":  "direct",
		"queue":          t.Name(),
		"routing_key":    t.Name(),
		"handler":        t.Name(),
		"auto_declare":   true,
		"auto_reconnect": true,
	}
}
