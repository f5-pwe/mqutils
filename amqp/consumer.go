package amqp

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/f5-pwe/mqutils"
)

type AMQPConsumer struct {
	Url                    string            `mapstructure:"url"`
	QueueName              string            `mapstructure:"queue"`
	RoutingKey             string            `mapstructure:"routing_key"`
	ExchangeName           string            `mapstructure:"exchange"`
	ExchangeType           string            `mapstructure:"exchange_type"`
	DeadLetterExchangeName string            `mapstructure:"dead_letter_exchange"`
	RetryQueueName         string            `mapstructure:"retry_queue_name"`
	RetryQueueTTL          int               `mapstructure:"retry_queue_ttl"`
	RetryQueueMaxRetries   int               `mapstructure:"retry_queue_max_retries"`
	HandlerName            string            `mapstructure:"handler"`
	AutoDeclare            bool              `mapstructure:"auto_declare"`
	Priority               string            `mapstructure:"priority"`
	AutoReconnect          bool              `mapstructure:"auto_reconnect"`
	Handler                mqutils.Handler   `mapstructure:"-"`
	Transport              mqutils.Transport `mapstructure:"-"`

	logger log.Logger
}

func NewAMQPConsumer(config map[string]interface{}, logger log.Logger) (mqutils.Consumer, error) {
	if logger == nil {
		logger = level.NewInjector(log.NewLogfmtLogger(os.Stderr), level.DebugValue())
		logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	}

	c := &AMQPConsumer{AutoDeclare: true, logger: logger}

	// Apply the configuration
	if err := mapstructure.Decode(config, c); err != nil {
		return nil, err
	}

	c.Transport = NewAMQPTransport()

	// Create an instance of the handler for the configured handler name
	newHandlerFunc, ok := mqutils.GetHandlerFunc(c.HandlerName)
	if !ok {
		return nil, fmt.Errorf("handler %s not found", c.HandlerName)
	}
	c.Handler = newHandlerFunc()

	// Apply the configuration to the handler instance
	if err := mapstructure.Decode(config, c.Handler); err != nil {
		return nil, err
	}

	return c, nil
}

// Run consumes messages for a consumer.
func (c *AMQPConsumer) Run(ctx context.Context) (err error) {
	if c.Transport == nil {
		return errors.New("AMQPMessage queue not configured")
	}

	if err = c.Handler.PreRun(); err != nil {
		return fmt.Errorf("PreRun: %w", err)
	}
	defer func() {
		if err1 := c.Handler.PostRun(); err1 != nil {
			level.Error(c.logger).Log("msg", "PostRun error", "err", err1)
		}
	}()

	if err = c.connect(ctx); err != nil {
		return err
	}

	delay := backoff.WithContext(backoff.NewExponentialBackOff(
		backoff.WithMaxElapsedTime(1*time.Hour),
		backoff.WithInitialInterval(1*time.Millisecond)), ctx)
	closeCh := c.Transport.(*AMQPTransport).channelClosed()
	messages, err := c.Transport.Messages(ctx, c.QueueName)
	if err != nil {
		return err
	}

	runLogger := log.With(c.logger,
		"exchange", c.ExchangeName,
		"queue", c.QueueName,
		"routing_key", c.RoutingKey,
	)

	level.Info(runLogger).Log("msg", "Listening for messages...")

	for {
		select {
		case tErr := <-closeCh:
			level.Error(runLogger).Log("msg", "Channel closed", "err", tErr)
			if c.AutoReconnect {
				sleep := delay.NextBackOff()
				level.Info(runLogger).Log("msg", "attempting reconnect", "sleep", sleep)
				if sleep == backoff.Stop {
					return errors.New("AMQPConsumer: reconnect timeout")
				}
				time.Sleep(sleep)
				if err = c.connect(ctx); err != nil {
					level.Error(runLogger).Log("msg", "Channel closed", "err", tErr)
					closeCh <- tErr
					continue
				}
				if messages, err = c.Transport.Messages(ctx, c.QueueName); err != nil {
					level.Error(runLogger).Log("msg", "message consumer error", "err", err)
					closeCh <- tErr
					continue
				}
				closeCh = c.Transport.(*AMQPTransport).channelClosed()
			}

		case <-ctx.Done():
			level.Info(runLogger).Log("msg", "AMQPConsumer stopping")
			if strings.Contains(ctx.Err().Error(), "context canceled") {
				return nil
			}

			level.Info(runLogger).Log("err", ctx.Err())
			err = ctx.Err()
			return err

		case message, ok := <-messages:
			if !ok {
				continue
			}

			msgLogger := message.Logger(runLogger)
			level.Info(msgLogger).Log("msg", "Received a message")

			// max retries reached - delete message without processing
			if c.RetryQueueMaxRetries > 0 && message.RetryCount() >= c.RetryQueueMaxRetries {
				level.Warn(msgLogger).Log("msg", "Delete message due to max retries")
				if err = c.Handler.PreDelete(message); err != nil {
					level.Warn(msgLogger).Log("msg", "Failed to call function before delete the message", "err", err)
				}
				if err = message.Ack(); err != nil {
					level.Warn(msgLogger).Log("msg", "Failed to acknowledge the message", "err", err)
				}
				continue
			}

			if err = c.Handler.Process(message); err != nil {
				if c.DeadLetterExchangeName != "" {
					// reject the message and move it to dlx
					if err = message.Nack(); err != nil {
						level.Warn(msgLogger).Log("msg", "Failed to negatively acknowledge the message", "err", err)
					}
				}
				level.Error(msgLogger).Log("msg", "Failed to consume the message", "err", err)
			}
		}
	}
}

func (c *AMQPConsumer) connect(ctx context.Context) (err error) {
	// Create a separate channel for each consumer
	if err = c.Transport.Dial(c.Url, c.logger); err != nil {
		return err
	}

	if c.AutoDeclare {
		if err = c.Transport.DeclareExchange(c.ExchangeName, c.ExchangeType); err != nil {
			err = errors.Wrapf(err, "Failed to declare exchange %s", c.ExchangeName)
			return err
		}

		queueArgs := make(map[string]interface{})

		// Create a dead letter exchange for failed messages
		if c.DeadLetterExchangeName != "" {
			if err = c.Transport.DeclareExchange(c.DeadLetterExchangeName, c.ExchangeType); err != nil {
				return errors.Wrapf(err, "Failed to declare exchange %s", c.DeadLetterExchangeName)
			}
			queueArgs["x-dead-letter-exchange"] = c.DeadLetterExchangeName

			if c.RetryQueueName != "" {
				if err = c.createRetryQueue(); err != nil {
					return err
				}
			}
		}

		if c.Priority != "" {
			if i, err := strconv.ParseInt(c.Priority, 10, 32); err == nil {
				queueArgs["x-max-priority"] = i
			}
		}

		if err = c.Transport.BindQueue(c.ExchangeName, c.QueueName, []string{c.RoutingKey}, queueArgs); err != nil {
			return errors.Wrapf(err, "Failed to bind queue %s on %s", c.QueueName, c.ExchangeName)
		}
	}

	return nil
}

func (c *AMQPConsumer) close() error {
	if c.Transport != nil {
		return c.Transport.Close()
	}
	return nil
}

func (c *AMQPConsumer) createRetryQueue() error {
	// point back to the original queue
	queueArgs := map[string]interface{}{
		"x-dead-letter-exchange": c.ExchangeName,
		"x-message-ttl":          int32(max(mqutils.DefaultRetryQueueTTL, c.RetryQueueTTL)),
	}

	if err := c.Transport.BindQueue(c.DeadLetterExchangeName, c.RetryQueueName, []string{c.RoutingKey}, queueArgs); err != nil {
		return errors.Wrapf(err, "Failed to bind retry queue %s on %s", c.RetryQueueName, c.DeadLetterExchangeName)
	}
	return nil
}
