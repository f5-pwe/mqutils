package amqp_test

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/f5-pwe/mqutils"
)

func newHandler() mqutils.Handler {
	return &amqpHandler{
		DefaultHandler: mqutils.DefaultHandler{},
		messages:       []mqutils.Message{},
		wg:             &sync.WaitGroup{},
		lock:           &sync.Mutex{},
	}
}

var _ mqutils.HandlerFunc = newHandler

type amqpHandler struct {
	mqutils.DefaultHandler
	messages []mqutils.Message
	wg       *sync.WaitGroup
	waiting  int
	lock     *sync.Mutex
	err      error
}

func (h *amqpHandler) PostRun() error {
	for !h.lock.TryLock() {
		time.Sleep(10 * time.Millisecond)
	}
	defer h.lock.Unlock()

	if h.waiting <= 0 || len(h.messages) == h.waiting {
		return nil
	}
	h.err = fmt.Errorf("failed to consume expected messages still waiting on: %d", h.waiting)

	for range h.waiting {
		h.wg.Done()
		// h.waiting--
	}
	return nil
}

func (h *amqpHandler) Process(m mqutils.Message) error {
	for !h.lock.TryLock() {
		time.Sleep(10 * time.Millisecond)
	}
	defer h.lock.Unlock()
	defer h.wg.Done()

	h.messages = append(h.messages, m)
	h.waiting--
	return nil
}

func (h *amqpHandler) waitFor(numMsgs int) {
	h.waiting = numMsgs
	h.wg.Add(numMsgs)
}
func (h *amqpHandler) wait() error {
	h.wg.Wait()

	return h.err
}
