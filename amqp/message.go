package amqp

import (
	"time"

	"github.com/go-kit/kit/log"
	"gitlab.com/f5-pwe/mqutils"
)

type AMQPMessage struct {
	Body         []byte
	Headers      map[string]interface{}
	Tag          uint64
	RoutingKey   string
	Acknowledged bool
	Retry        int

	// Properties
	ContentType     string    // MIME content type
	ContentEncoding string    // MIME content encoding
	DeliveryMode    uint8     // queue implemention use - non-persistent (1) or persistent (2)
	Priority        uint8     // queue implementation use - 0 to 9
	CorrelationId   string    // application use - correlation identifier
	ReplyTo         string    // application use - address to to reply to (ex: RPC)
	Expiration      string    // implementation use - message expiration spec
	MessageId       string    // application use - message identifier
	Timestamp       time.Time // application use - message timestamp
	Type            string    // application use - message type name
	UserId          string    // application use - creating user - should be authenticated user
	AppId           string    // application use - creating application id

	Transport mqutils.Transport `json:"-"`
}

func (m *AMQPMessage) Ack() error {
	if m.Acknowledged {
		return nil
	}
	if err := m.Transport.Ack(m); err != nil {
		return err
	}
	m.Acknowledged = true
	return nil
}

func (m *AMQPMessage) Nack() error {
	if m.Acknowledged {
		return nil
	}
	if err := m.Transport.Nack(m); err != nil {
		return err
	}
	m.Acknowledged = true
	return nil
}

func (m *AMQPMessage) RetryCount() int {
	return m.Retry
}

func (m *AMQPMessage) Logger(baseLogger log.Logger) (logger log.Logger) {
	logger = log.With(baseLogger,
		"content", string(m.Body),
		"content_length", len(m.Body),
	)

	if m.CorrelationId != "" {
		logger = log.With(logger,
			"correlation_id", m.CorrelationId,
		)
	}

	if m.MessageId != "" {
		logger = log.With(logger,
			"message_id", m.MessageId,
		)
	}

	if m.UserId != "" {
		logger = log.With(logger,
			"user_id", m.UserId,
		)
	}

	if m.AppId != "" {
		logger = log.With(logger,
			"app_id", m.AppId,
		)
	}

	if txid, ok := m.Headers["txid"]; ok {
		logger = log.With(logger,
			"txid", txid,
		)
	}

	if version, ok := m.Headers["client_version"]; ok {
		logger = log.With(logger,
			"client_version", version,
		)
	}
	if hostname, ok := m.Headers["hostname"]; ok {
		logger = log.With(logger,
			"hostname", hostname,
		)
	}

	if m.Retry > 0 {
		logger = log.With(logger,
			"retry", m.Retry,
		)
	}

	return
}
