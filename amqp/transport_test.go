package amqp_test

import (
	"context"
	"testing"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/avarf/getenvs"
	"gitlab.com/f5-pwe/mqutils"
	"gitlab.com/f5-pwe/mqutils/amqp"
)

var rmq_host = getenvs.GetEnvString("TEST_RMQ_HOST", "amqp://guest:guest@localhost:5672/")

func TestRabbitMQTransport_Dial(t *testing.T) {
	t.Parallel()

	transport := newTransport(t)
	assert.NoError(t, transport.Dial(rmq_host, testLogger(t)))
}

func TestRabbitMQTransport_DeclareExchange(t *testing.T) {
	t.Parallel()

	transport := newTransport(t)
	err := transport.Dial(rmq_host, testLogger(t))
	assert.NoError(t, err)
	err = transport.DeclareExchange(t.Name(), "direct")
	assert.NoError(t, err)
}

func TestRabbitMQTransport_BindQueue(t *testing.T) {
	t.Parallel()

	transport := newTransport(t)
	err := transport.Dial(rmq_host, testLogger(t))
	require.NoError(t, err)
	err = transport.DeclareExchange(t.Name(), "direct")
	assert.NoError(t, err)
	err = transport.BindQueue(t.Name(), t.Name(), []string{t.Name()}, nil)
	assert.NoError(t, err)
}

func TestRabbitMQTransport_Messages(t *testing.T) {
	t.Parallel()

	transport := newTransport(t)
	err := transport.Dial(rmq_host, testLogger(t))
	assert.NoError(t, err)
	err = transport.DeclareExchange(t.Name(), "direct")
	assert.NoError(t, err)
	err = transport.BindQueue(t.Name(), t.Name(), []string{t.Name()}, nil)
	assert.NoError(t, err)

	msgs, err := transport.Messages(context.Background(), t.Name())
	assert.NoError(t, err)
	send(t, rmq_host, "test")

	msg := <-msgs
	msg.Logger(testLogger(t)).Log("msg", msg)
	assert.IsType(t, &amqp.AMQPMessage{}, msg)
	assert.Equal(t, "test", string(msg.(*amqp.AMQPMessage).Body))
}

func newTransport(t *testing.T) mqutils.Transport {
	t.Helper()

	transport := amqp.NewAMQPTransport()
	assert.IsType(t, &amqp.AMQPTransport{}, transport)

	t.Cleanup(func() {
		err := transport.Close()
		assert.NoError(t, err)
	})

	return transport
}

func send(t *testing.T, url, msg string) {
	t.Helper()
	conn, err := amqp091.Dial(url)
	require.NoError(t, err)
	defer conn.Close()
	ch, err := conn.Channel()
	require.NoError(t, err)
	defer ch.Close()
	err = ch.Publish(t.Name(), t.Name(), false, false, amqp091.Publishing{
		ContentType: "text/plain",
		Body:        []byte(msg),
	})
	require.NoError(t, err)
}
