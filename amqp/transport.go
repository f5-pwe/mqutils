package amqp

import (
	"context"
	"fmt"
	"os"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/f5-pwe/mqutils"
)

var (
	amqpPool *sync.Map
	once     = &sync.Once{}
)

func dialAMQP(url string, logger log.Logger) (*amqp091.Channel, error) {
	conn, err := getAMQPConnection(url, logger)
	if err != nil {
		return nil, err
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	// Use fair dispatching instead of round-robin, if the worker is busy
	// the message will be dispatched to another worker.
	if err = channel.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	); err != nil {
		return nil, err
	}

	return channel, nil
}

// NewAMQPTransport creates a new AMQP transport.
func NewAMQPTransport() mqutils.Transport {
	return &AMQPTransport{wg: &sync.WaitGroup{}}
}

type AMQPTransport struct {
	sync.RWMutex
	channel *amqp091.Channel
	logger  log.Logger
	wg      *sync.WaitGroup
}

func (t *AMQPTransport) Dial(url string, logger log.Logger) (err error) {
	t.Lock()
	defer t.Unlock()
	if logger == nil {
		logger = level.NewInjector(log.NewLogfmtLogger(os.Stderr), level.DebugValue())
		logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	}
	t.logger = logger
	t.channel, err = dialAMQP(url, logger)

	return
}

func (t *AMQPTransport) Close() error {
	t.RLock()
	defer t.RUnlock()

	if t.channel != nil {
		return t.channel.Close()
	}
	t.wg.Wait()
	return nil
}

func (t *AMQPTransport) Ack(message mqutils.Message) error {
	t.RLock()
	defer t.RUnlock()

	return t.channel.Ack(message.(*AMQPMessage).Tag, false)
}

func (t *AMQPTransport) Nack(message mqutils.Message) error {
	t.RLock()
	defer t.RUnlock()

	return t.channel.Nack(message.(*AMQPMessage).Tag, false, false)
}

func (t *AMQPTransport) DeclareExchange(exchangeName, exchangeType string) error {
	t.RLock()
	defer t.RUnlock()

	return t.channel.ExchangeDeclare(
		exchangeName, // name
		exchangeType, // type
		true,         // durable
		false,        // delete when unused
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)
}

func (t *AMQPTransport) BindQueue(exchangeName, queueName string, routingKeys []string, args map[string]interface{}) error {
	t.RLock()
	defer t.RUnlock()

	if _, err := t.channel.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		args,      // arguments
	); err != nil {
		return fmt.Errorf("declare error: %v", err)
	}

	for _, routingKey := range routingKeys {
		if err := t.channel.QueueBind(
			queueName,    // name
			routingKey,   // routing key
			exchangeName, // exchange
			false,        // no-wait
			nil,          // arguments
		); err != nil {
			return fmt.Errorf("bind error: %v", err)
		}
	}
	return nil
}

// Translate AMQP deliveries into messages.
func (t *AMQPTransport) Messages(ctx context.Context, queueName string) (<-chan mqutils.Message, error) {
	t.RLock()
	defer t.RUnlock()

	messages := make(chan mqutils.Message)

	deliveries, err := t.channel.Consume(
		queueName, // queue
		queueName, // consumer tag
		false,     // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		return nil, err
	}

	t.wg.Add(1)
	go func() {
		defer t.wg.Done()
		defer func() {
			close(messages)
			_ = t.Close()
		}()
		for {
			select {
			case <-t.channelClosed():
				return
			case <-ctx.Done():
				return
			case d := <-deliveries:
				if d.Acknowledger != nil {
					message := &AMQPMessage{
						Body:         d.Body,
						Headers:      d.Headers,
						Tag:          d.DeliveryTag,
						RoutingKey:   d.RoutingKey,
						Acknowledged: false,
						Retry:        getRetryCount(d, queueName),

						ContentType:     d.ContentType,
						ContentEncoding: d.ContentEncoding,
						DeliveryMode:    d.DeliveryMode,
						Priority:        d.Priority,
						CorrelationId:   d.CorrelationId,
						ReplyTo:         d.ReplyTo,
						Expiration:      d.Expiration,
						MessageId:       d.MessageId,
						Timestamp:       d.Timestamp,
						Type:            d.Type,
						UserId:          d.UserId,
						AppId:           d.AppId,

						Transport: t,
					}
					messages <- message
				}
			}
		}
	}()

	return messages, nil
}

func (t *AMQPTransport) channelClosed() chan *amqp091.Error {
	t.RLock()
	defer t.RUnlock()

	return t.channel.NotifyClose(make(chan *amqp091.Error, 1))
}

func getRetryCount(d amqp091.Delivery, queueName string) int {
	if v, ok := d.Headers["x-death"]; ok {
		if retries, ok := v.([]interface{}); ok {
			// message must go through original and then retry queue
			// where it first gets rejected and then expired

			count := 0
			for _, r := range retries {
				if retry, ok := r.(amqp091.Table); ok {
					if retry["queue"] == queueName {
						if c, ok := retry["count"].(int64); ok {
							count += int(c)
						} else {
							count += 1
						}
					}
				}
			}

			return count
		}
	}
	return 0
}

func reconnectClosed(url string, conn *amqp091.Connection, logger log.Logger) {
	errCh := conn.NotifyClose(make(chan *amqp091.Error, 1))

	go func() {
		// Block until a closed connection notification arrives
		err := <-errCh
		level.Error(logger).Log("msg", "connection closed", "err", err)
		// Force recreating by removing from a map of opened connections
		amqpPool.Delete(url)
	}()
}

func handleBlocked(conn *amqp091.Connection, logger log.Logger) {
	blockCh := conn.NotifyBlocked(make(chan amqp091.Blocking))

	go func() {
		for b := range blockCh {
			if b.Active {
				level.Info(logger).Log("msg", "connection blocked", "reason", b.Reason)
			} else {
				level.Info(logger).Log("msg", "connection unlocked")
			}
		}
	}()
}

func getAMQPConnection(url string, logger log.Logger) (*amqp091.Connection, error) {
	once.Do(func() {
		amqpPool = &sync.Map{}
	})

	if _, ok := amqpPool.Load(url); !ok {
		connectionLogger := log.With(logger, "url", url)
		level.Info(connectionLogger).Log("msg", "connecting")
		conn, err := amqp091.Dial(url)
		if err != nil {
			return nil, err
		}
		amqpPool.Store(url, conn)
		reconnectClosed(url, conn, connectionLogger)
		handleBlocked(conn, connectionLogger)
	}
	conn, _ := amqpPool.Load(url)
	return conn.(*amqp091.Connection), nil
}
